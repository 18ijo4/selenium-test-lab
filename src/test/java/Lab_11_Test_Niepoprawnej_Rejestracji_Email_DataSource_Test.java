import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.CreateAccountPage;
import pages.LoginPage;

public class Lab_11_Test_Niepoprawnej_Rejestracji_Email_DataSource_Test extends SeleniumBaseTest {

    @DataProvider
    public Object[][] getWrongEmails() {
        return new Object[][]{
                {"test"},
                {"admin"},
                {"test"}
        };
    }

    @Test(dataProvider = "getWrongEmails")
    public void incorrectRegistration(String wrongEmail) {

        LoginPage loginPage = new LoginPage(driver);
        CreateAccountPage createAccountPage = loginPage.goToRegisterPage();
        createAccountPage.typeEmail(wrongEmail);
        createAccountPage.registerWithFailure();
        createAccountPage.assertEmailError("The Email field is not a valid e-mail address.");

    }
    @Test
    public void incorrectRegistrationWithChaining() {
        new CreateAccountPage(driver)
                .typeEmail("test")
                .registerWithFailure()
                .assertEmailError("The Email field is not a valid e-mail address.");
    }
}





