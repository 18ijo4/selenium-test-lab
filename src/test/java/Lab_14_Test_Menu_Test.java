import config.Config;
import org.testng.annotations.Test;
import pages.LoginPage;

public class Lab_14_Test_Menu_Test extends SeleniumBaseTest {

    @Test
    public void menuTest() {
        new LoginPage(driver)
                .typeEmail(config.getApplicationUser())
                .typePassword(config.getApplicationPassword())
                .submitLogin()
                .goToProcesses()
                .goToCharacteristics()
                .goToDashboard();

    }
}