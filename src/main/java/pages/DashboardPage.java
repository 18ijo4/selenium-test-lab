package pages;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class DashboardPage extends HomePage {

    public DashboardPage(WebDriver driver) {
        super(driver);
    }
    public DashboardPage assertDashboardUrl() {
        Assert.assertTrue(driver.getCurrentUrl().contains("DEMO CHARACTERISTIC"));
        return this;
    }
}
