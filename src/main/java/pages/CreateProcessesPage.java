package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class CreateProcessesPage extends ProcessesPage {

    public CreateProcessesPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "Name")
    private WebElement nameTxt;

    @FindBy(css = "input[type=submit]")
    private WebElement createBtn;

    @FindBy(css = ".field-validation-error[data-valmsg-for=Name]")
    private WebElement nameError;

    public CreateProcessesPage typeName(String processName) {
        nameTxt.clear();
        nameTxt.sendKeys(processName);
        return this;
    }

    public ProcessesPage submitCreate() {
        createBtn.click();
        return new ProcessesPage(driver);
    }

    public ProcessesPage submitCreateWithFailure() {
        createBtn.click();
        return this;    }

    public CreateProcessesPage assertProcessNameError(String expErrorMessage) {
        Assert.assertEquals(nameError.getText(), expErrorMessage);
        return this;
    }
}

