package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.List;

public class CreateAccountPage {
    protected WebDriver driver;

    public CreateAccountPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "Email")
    private WebElement emailTxt;

    @FindBy(id = "Password")
    private WebElement passwordTxt;

    @FindBy(css = "#Email-error")
    public WebElement emailError;

    @FindBy(css = "button[type=submit]")
    private WebElement registerBtn;

    public CreateAccountPage typeEmail(String email) {
        emailTxt.clear();
        emailTxt.sendKeys(email);
        return this;
    }

    public CreateAccountPage typePassword(String password) {
        passwordTxt.clear();
        passwordTxt.sendKeys(password);
        return this;
    }

    public HomePage register() {
        registerBtn.click();
        return new HomePage(driver);
    }

    public CreateAccountPage registerWithFailure() {
        registerBtn.click();
        return this;
    }
    public CreateAccountPage assertEmailError(String errorMessage) {
        Assert.assertTrue(emailError.getText().contains(errorMessage));
        return this;
    }
}






