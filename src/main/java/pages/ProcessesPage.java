package pages;

import config.Config;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.List;

public class ProcessesPage extends HomePage {

    private String URL = new Config().getApplicationUrl() + "Processes";
    private String GENERIC_PROCESS_ROW_XPATH = "//tbody//td[text()='%s']/..";

    public ProcessesPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(linkText = "Add new process")
    private WebElement addBtn;

    @FindBy(css = "a.btn[href*=Projects]")
    private WebElement backToListBtn;

    public CreateProcessesPage clickAddProcess() {
        addBtn.click();
        return new CreateProcessesPage(driver);
    }

    public ProcessesPage assertProcessesUrl() {
        Assert.assertTrue(driver.getCurrentUrl().contains("Processes"));
        return this;
    }

    public ProcessesPage assertProcess(String expName, String expDescription, String expNotes) {
        String processXpath = String.format(GENERIC_PROCESS_ROW_XPATH, expName);
        WebElement processRow = driver.findElement(By.xpath(processXpath));
        String actDescription = processRow.findElement(By.xpath("./td[2]")).getText();
        String actNotes = processRow.findElement(By.xpath("./td[3]")).getText();
        Assert.assertEquals(actDescription, expDescription);
        Assert.assertEquals(actNotes, expNotes);
        return this;
    }

    public ProcessesPage assertProcessNameError(String expErrorMessage) {
        String processXpath = String.format(GENERIC_PROCESS_ROW_XPATH, expErrorMessage);
        WebElement processRow = driver.findElement(By.xpath(processXpath));
        String actDescription = processRow.findElement(By.xpath("./td[2]")).getText();
        String actNotes = processRow.findElement(By.xpath("./td[3]")).getText();
        Assert.assertEquals(expErrorMessage, expErrorMessage);
        return this;
    }

    public ProcessesPage backToList() {
        backToListBtn.click();
        return new ProcessesPage(driver);
    }

    public ProcessesPage assertProcessIsNotShown(String processName) {
        String processXpath = String.format(GENERIC_PROCESS_ROW_XPATH, processName);
        List<WebElement> process = driver.findElements(By.xpath(processXpath));
        Assert.assertEquals(process.size(), 0);
        return this;
    }
}






