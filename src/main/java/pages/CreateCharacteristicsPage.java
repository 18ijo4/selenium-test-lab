package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CreateCharacteristicsPage extends CharacteristicsPage {

    public CreateCharacteristicsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "ProjectId")
    private WebElement projectSlc;

    @FindBy (id = "Name")
    private WebElement nameTxt;

    @FindBy (id = "LowerLimit")
    private WebElement lslTxt;

    @FindBy (id = "UpperLimit")
    private WebElement ulsTxt;

    @FindBy (id = "Count")
    private WebElement countTxt;

    public CreateCharacteristicsPage typeName(String name) {
        nameTxt.clear();
        nameTxt.sendKeys(name);
        return this;
    }

    public CreateCharacteristicsPage typeLsl(String lowerLimit) {
        lslTxt.clear();
        lslTxt.sendKeys(lowerLimit);
        return this;
    }
    public CreateCharacteristicsPage typeUls(String upperLimit) {
        ulsTxt.clear();
        ulsTxt.sendKeys(upperLimit);
        return this;
    }
}
